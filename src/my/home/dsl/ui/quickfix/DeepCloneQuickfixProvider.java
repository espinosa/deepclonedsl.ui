package my.home.dsl.ui.quickfix;

import my.home.dsl.deepClone.ContainerType;
import my.home.dsl.deepClone.DeepCloneFactory;
import my.home.dsl.deepClone.SimpleField;
import my.home.dsl.utils.ReflectionUtils;
import my.home.dsl.validation.DeepCloneJavaValidator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

import com.google.common.collect.Iterables;

/**
 * QuickFix provider based on ISemanticModification.
 * <p>
 * There is known issues with broken formatting after the change in xtext 2.3.1
 * 
 * @author espinosa
 */
public class DeepCloneQuickfixProvider extends DefaultQuickfixProvider {

	@Fix(DeepCloneJavaValidator.MISSING_FIELD_ERROR)
	public void addMissingField(final Issue issue, IssueResolutionAcceptor acceptor) {
		final String missingFieldName = issue.getData()[0];
		acceptor.accept(issue, "Add missing field " + missingFieldName, "Add missing field " + missingFieldName, null, new ISemanticModification() {
			public void apply(EObject element, IModificationContext context) {
				DeepCloneFactory deepCloneFactory = DeepCloneFactory.eINSTANCE;
				SimpleField field = deepCloneFactory.createSimpleField();
				field.setFieldName(missingFieldName);
				((ContainerType)element).getFields().add(field);
			}
		});
	}

	@Fix(DeepCloneJavaValidator.MISSING_FIELDS_ERROR)
	public void addMissingFields(final Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, "Add all missing fields", "Add all missing fields", null, new ISemanticModification() {
			public void apply(EObject element, IModificationContext context) {
				DeepCloneFactory deepCloneFactory = DeepCloneFactory.eINSTANCE;
				for (String missingFieldName : issue.getData()) {
					SimpleField field = deepCloneFactory.createSimpleField();
					field.setFieldName(missingFieldName);
					((ContainerType)element).getFields().add(field);
				}
			}
		});
	}

	@Fix(DeepCloneJavaValidator.SURPLUS_FIELD_ERROR)
	public void removeSurplusField(final Issue issue, IssueResolutionAcceptor acceptor) {
		final String surplusFieldName = issue.getData()[0];
		acceptor.accept(issue, "Remove surplus field " + surplusFieldName, "Remove surplus field " + surplusFieldName, null, new ISemanticModification() {
			public void apply(EObject element, IModificationContext context) {
				Iterables.removeIf(
						((ContainerType)element.eContainer()).getFields(), 
						ReflectionUtils.byName(surplusFieldName));
			}
		});
	}

	@Fix(DeepCloneJavaValidator.SURPLUS_FIELDS_ERROR)
	public void removeSurplusFields(final Issue issue, IssueResolutionAcceptor acceptor) {
		acceptor.accept(issue, "Remove all surplus fields", "Remove all surplus fields", null, new ISemanticModification() {
			public void apply(EObject element, IModificationContext context) {
				for (String surplusFieldName : issue.getData()) {
					Iterables.removeIf(
							((ContainerType)element.eContainer()).getFields(), 
							ReflectionUtils.byName(surplusFieldName));
				}
			}
		});
	}
}
