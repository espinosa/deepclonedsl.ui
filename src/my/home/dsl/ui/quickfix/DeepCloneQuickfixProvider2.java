package my.home.dsl.ui.quickfix;

import my.home.dsl.validation.DeepCloneJavaValidator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.xtext.nodemodel.BidiIterator;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.eclipse.xtext.ui.editor.model.IXtextDocument;
import org.eclipse.xtext.ui.editor.model.edit.IModification;
import org.eclipse.xtext.ui.editor.model.edit.IModificationContext;
import org.eclipse.xtext.ui.editor.model.edit.ISemanticModification;
import org.eclipse.xtext.ui.editor.quickfix.DefaultQuickfixProvider;
import org.eclipse.xtext.ui.editor.quickfix.Fix;
import org.eclipse.xtext.ui.editor.quickfix.IssueResolutionAcceptor;
import org.eclipse.xtext.validation.Issue;

/**
 * Alternative QuickFix provider based on {@link ISemanticModification} but with
 * direct text document manipulation. Main reason to create this alternative was
 * to get around bug in formatting after ISemanticModification/AST changes
 * experienced in {@link DeepCloneQuickfixProvider}. It does not not help (easily) 
 * with the issue and code is more clunky so I abandoned this approach. 
 * <p>
 * Originally this provider was extending just {@link IModification}. But then 
 * {@link IModification#apply(IModificationContext)} method hands in only context and
 * one has to find troubled element himself, based on the {@link Issue}. Extending  
 * {@link ISemanticModification} brings handily extended apply() method providing 
 * the troubled {@link EObject} straight away.
 * 
 * @author espinosa
 */
@Deprecated
public class DeepCloneQuickfixProvider2 extends DefaultQuickfixProvider {

	@Fix(DeepCloneJavaValidator.MISSING_FIELD_ERROR)
	public void addMissingField(final Issue issue, IssueResolutionAcceptor acceptor) {
		final String missingFieldName = issue.getData()[0];
		acceptor.accept(issue, "Add missing field " + missingFieldName, "Add missing field " + missingFieldName, null, new ISemanticModification() {
			public void apply(EObject element, IModificationContext context) {
				IXtextDocument xtextDocument = context.getXtextDocument();
				try {
					// transform EObject (AST) to INode. Node contains position in the text document information
					ICompositeNode node = NodeModelUtils.getNode(element);
					BidiIterator<INode> i = node.getChildren().iterator();
					while (i.hasNext()) {
						INode n = i.next();
						// locate enclosing '}' token, we will insert text just before it, and some line breaks 
						if (n.getText().equals("}")) {
							// there is no insert() method, only replace(), but this variant of replace() in fact inserts text
							// Insert missing field name directly to the text of the document
							// parser will soon after fix model (AST)
							xtextDocument.replace(n.getOffset(), 0, "\n"+missingFieldName+"\n");
						}
					}
				} catch (BadLocationException e) {
					throw new RuntimeException(e); 
				}
			}
		});
	}
}
